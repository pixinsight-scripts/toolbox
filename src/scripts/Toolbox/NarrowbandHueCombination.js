// NarrowbandHueCombination
// version 1.50 03/06/2024
// by Jürgen Bätz
// using PreviewControl.js by PI Team

#include <pjsr/ColorSpace.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/StdCursor.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/ColorComboBox.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/CheckState.jsh>


#feature-id NarrowbandHueCombination : Toolbox > NarrowbandHueCombination

#feature-info PI-Script to simplify the HSV colourspace-based generation of RGB images from narrowband sources. <br />  \
    <br />  \
    This script blends H-alpha and optionally OIII and SII grayscale narrowband and starry RGB broadband images using the screen method into a resulting RGB image.<br />\
    The grayscale images will be color toned in HSV color space using their H(ue).<br />Use <i>starless</i> narrowband images for best results.<br />\
    <br />Copyright &copy; 2022-2024 J&uuml;rgen B&auml;tz

#define VERSION         "1.50"
#define SVIEWID         "Sat"
#define HHAVIEWID       "HueHa"
#define HO3VIEWID       "HueOIII"
#define HS2VIEWID       "HueSII"
#define COLHAVIEWID     "colorHa"
#define COLO3VIEWID     "colorOIII"
#define COLS2VIEWID     "colorSII"
#define OUTPUTVIEWID    "NarrowbandHueCombinationView"
#define DEFAULT_AUTOSTRETCH_SCLIP  -1.25
#define DEFAULT_AUTOSTRETCH_TBGND   0.25
#define DEFAULT_AUTOSTRETCH_CLINK   false
#define SCALEDWIDTH                 1200
#define SCALEDRIGHTWIDTH            450

var previewed = false;
var autostf = false;
var linkedstf = false;

// define a global variable containing script's parameters
var gParameters = {
    view1Hue: 0,
    view2Hue: 180,
    view3Hue: 30,
    view1Amount: 1,
    view2Amount: 1,
    view3Amount: 1,
    view4Amount: 0.5,
    scnrAmount: 0,
    view1: undefined,
    view2: undefined,
    view3: undefined,
    view4: undefined,
    linkedStretch: false,
    autoStretch: false,
    imageActiveWindow: undefined,
    workingPreview: undefined,
    doneButtonPressed: false,

    // stores the current parameters values into the script instance
    save: function () {
        if (gParameters.view1) Parameters.set("view1", gParameters.view1.id);
        Parameters.set("view1Hue", gParameters.view1Hue);
        Parameters.set("view1Amount", gParameters.view1Amount);
        if (gParameters.view2) Parameters.set("view2", gParameters.view2.id);
        Parameters.set("view2Hue", gParameters.view2Hue);
        Parameters.set("view2Amount", gParameters.view2Amount);
        if (gParameters.view3) Parameters.set("view3", gParameters.view3.id);
        Parameters.set("view3Hue", gParameters.view3Hue);
        Parameters.set("view3Amount", gParameters.view3Amount);
        if (gParameters.view4) Parameters.set("view4", gParameters.view4.id);
        Parameters.set("view4Amount", gParameters.view4Amount);
        Parameters.set("scnrAmount", gParameters.scnrAmount);
        Parameters.set("linkedStretch", gParameters.linkedStretch);
        Parameters.set("autoStretch", gParameters.autoStretch);
    },

    // loads the script instance parameters
    load: function () {
        if (Parameters.has("view1")) {
            var id = Parameters.getString("view1");
            gParameters.view1 = View.viewById(id);
        }
        if (Parameters.has("view1Hue"))
            gParameters.view1Hue = Parameters.getReal("view1Hue")
        if (Parameters.has("view1Amount"))
            gParameters.view1Amount = Parameters.getReal("view1Amount")
        if (Parameters.has("view2")) {
            var id = Parameters.getString("view2");
            gParameters.view2 = View.viewById(id);
        }
        if (Parameters.has("view2Hue"))
            gParameters.view2Hue = Parameters.getReal("view2Hue")
        if (Parameters.has("view2Amount"))
            gParameters.view2Amount = Parameters.getReal("view2Amount")
        if (Parameters.has("view3")) {
            var id = Parameters.getString("view3");
            gParameters.view3 = View.viewById(id);
        }
        if (Parameters.has("view3Hue"))
            gParameters.view3Hue = Parameters.getReal("view3Hue")
        if (Parameters.has("view3Amount"))
            gParameters.view3Amount = Parameters.getReal("view3Amount")
        if (Parameters.has("view4")) {
            var id = Parameters.getString("view4");
            gParameters.view4 = View.viewById(id);
        }
        if (Parameters.has("view4Amount"))
            gParameters.view4Amount = Parameters.getReal("view4Amount")
        if (Parameters.has("scnrAmount"))
            gParameters.scnrAmount = Parameters.getReal("scnrAmount")
        if (Parameters.has("linkedStretch"))
            gParameters.linkedStretch = Parameters.getBoolean("linkedStretch")
        if (Parameters.has("autoStretch"))
            gParameters.autoStretch = Parameters.getBoolean("autoStretch")
    }
}

// do all the magic:
// 1. create grayscale HSV component images (saturation, hues) and combine them to (monochrome) color images using ChannelCombination.
// 2. Screen-blend the images using PixelMath

function DoMerge(view1, view2, view3, view4, view1hue, view2hue, view3hue, view1amount, view2amount, view3amount, view4amount) {

    var width = view1.image.width;
    var height = view1.image.height;
    var hue1 = view1hue / 360;
    var hue2 = view2hue / 360;
    var hue3 = view3hue / 360;
    var expression = "";
    var expression_view2 = "";
    var expression_view3 = "";
    var expression_view4 = "";

    // create saturation channel
    var satimg = new ImageWindow(width, height, 1, 32, true, false, SVIEWID);
    with (satimg.mainView) {
        beginProcess(UndoFlag_NoSwapFile);
        image.fill(1); // sat 100%
        endProcess();
    }
    var sView = View.viewById(SVIEWID);

    // create hue channel for view #1
    var hHaimg = new ImageWindow(width, height, 1, 32, true, false, HHAVIEWID);
    with (hHaimg.mainView) {
        beginProcess(UndoFlag_NoSwapFile);
        image.fill(hue1); // variation hue Ha
        endProcess();
    }
    var hHaView = View.viewById(HHAVIEWID);

    if (!view2.isNull) {
        // create hue channel for view #2
        var hOIIIimg = new ImageWindow(width, height, 1, 32, true, false, HO3VIEWID);
        with (hOIIIimg.mainView) {
            beginProcess(UndoFlag_NoSwapFile);
            image.fill(hue2); // variation hue OIII
            endProcess();
        }
        var hOIIIView = View.viewById(HO3VIEWID);
    }

    if (!view3.isNull) {
        // create hue channel for view #3
        var hSIIimg = new ImageWindow(width, height, 1, 32, true, false, HS2VIEWID);
        with (hSIIimg.mainView) {
            beginProcess(UndoFlag_NoSwapFile);
            image.fill(hue3); // variation hue SII
            endProcess();
        }
        var hSIIView = View.viewById(HS2VIEWID);
    }

    // generate the colorized monochrome temporary image for view #1
    var colorHaimg = new ImageWindow(width, height, 3, 32, true, true, COLHAVIEWID);
    with (colorHaimg.mainView) {
        beginProcess(UndoFlag_NoSwapFile);
        image.fill(0);
        endProcess();
    }
    var colorHaView = View.viewById(COLHAVIEWID);

    var P = new ChannelCombination;
    P.colorSpace = ChannelCombination.prototype.HSV;
    P.channels = [// enabled, id
        [true, hHaView.id],
        [true, sView.id],
        [true, view1.id]
    ];
    P.executeOn(colorHaView, false);

    // generate the colorized monochrome temporary image for view #2
    if (!view2.isNull) {
        var colorOIIIimg = new ImageWindow(width, height, 3, 32, true, true, COLO3VIEWID);
        with (colorOIIIimg.mainView) {
            beginProcess(UndoFlag_NoSwapFile);
            image.fill(0);
            endProcess();
        }
        var colorOIIIView = View.viewById(COLO3VIEWID);

        var P = new ChannelCombination;
        P.colorSpace = ChannelCombination.prototype.HSV;
        P.channels = [// enabled, id
            [true, hOIIIView.id],
            [true, sView.id],
            [true, view2.id]
        ];
        P.executeOn(colorOIIIView, false);

        expression_view2 = " * ~(" + view2amount + " * " + COLO3VIEWID + ")";
    }

    // generate the colorized monochrome temporary image for view #3
    if (!view3.isNull) {
        var colorSIIimg = new ImageWindow(width, height, 3, 32, true, true, COLS2VIEWID);
        with (colorSIIimg.mainView) {
            beginProcess(UndoFlag_NoSwapFile);
            image.fill(0);
            endProcess();
        }
        var colorSIIView = View.viewById(COLS2VIEWID);

        var P = new ChannelCombination;
        P.colorSpace = ChannelCombination.prototype.HSV;
        P.channels = [// enabled, id
            [true, hSIIView.id],
            [true, sView.id],
            [true, view3.id]
        ];
        P.executeOn(colorSIIView, false);

        expression_view3 = " * ~(" + view3amount + " * " + COLS2VIEWID + ")";
    }

    // contribute the RGB image transformed with mtf to lower the stars presence
    if (!view4.isNull) {
        expression_view4 = " * ~(mtf(" + view4amount + "," + view4.id + "))";
    }

    expression = "~(~(" + view1amount + "* " + COLHAVIEWID + ")" + expression_view2 + expression_view3 + expression_view4 + ");";

    var P = new PixelMath;
    P.expression = expression;
    P.expression1 = "";
    P.expression2 = "";
    P.expression3 = "";
    P.useSingleExpression = true;
    P.symbols = "";
    P.clearImageCacheAndExit = false;
    P.cacheGeneratedImages = false;
    P.generateOutput = true;
    P.singleThreaded = false;
    P.optimization = true;
    P.use64BitWorkingImage = false;
    P.rescale = false;
    P.rescaleLower = 0;
    P.rescaleUpper = 1;
    P.truncate = true;
    P.truncateLower = 0;
    P.truncateUpper = 1;
    P.createNewImage = false;
    P.showNewImage = true;
    P.executeOn(gParameters.workingPreview.mainView, false);

    var SCNRProcess = new SCNR;
    SCNRProcess.amount = gParameters.scnrAmount;
    SCNRProcess.colorToRemove = SCNR.prototype.Green;
    SCNRProcess.protectionMode = SCNR.prototype.AverageNeutral;
    SCNRProcess.preserveLightness = true;
    SCNRProcess.executeOn(gParameters.workingPreview.mainView, false);

    // clean up
    sView.window.forceClose();
    hHaView.window.forceClose();
    colorHaView.window.forceClose();
    if (!view2.isNull) {
        hOIIIView.window.forceClose();
        colorOIIIView.window.forceClose();
    }
    if (!view3.isNull) {
        hSIIView.window.forceClose();
        colorSIIView.window.forceClose();
    }
}

/*
 * Construct the script dialog interface
 */
function NarrowbandHueCombinationDialog() {
    this.__base__ = Dialog;
    this.__base__();

    this.userResizable = false;
    this.scaledMinWidth = SCALEDWIDTH;

    var error = false;
    var col = 0;

    this.title = new Label( this );
    this.title.frameStyle = FrameStyle_Box;
    this.title.margin = 6;
    this.title.wordWrapping = true;
    this.title.useRichText = true;
    this.title.text = "<b>NarrowbandHueCombination v" + VERSION + "</b><br><br>This script blends H-alpha and optionally OIII and SII grayscale narrowband and starry RGB broadband images using the screen method to create a RGB image. \
    The grayscale images will be color toned in HSV color space using their H(ue).<br>Use <i>starless</i> narrowband images for best results.\
    <br><br>Copyright &copy; 2022-2024 J&uuml;rgen B&auml;tz";

    // initialize preview with dummy data

    gParameters.workingPreview = new ImageWindow(1, 1, 3, 32, true, true, OUTPUTVIEWID);

    var i = new Image(4000, 4000, 3, 1, 32, 1);
    gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
    gParameters.workingPreview.mainView.image.assign(i);
    gParameters.workingPreview.mainView.endProcess();
    var metadata = {
        width: 4000,
        height: 4000
    }

    this.previewControl = new PreviewControl(this);
    this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);

    //  H-alpha view selection and controls

    this.view1Label = new Label(this);
    this.view1Label.text = "View:";
    this.view1List = new ViewList(this);
    this.view1List.getMainViews();
    this.view1List.remove(View.viewById(OUTPUTVIEWID));
    if (gParameters.view1)
        this.view1List.currentView = gParameters.view1;
    this.view1List.onViewSelected = function (view) {
        gParameters.view1 = view;
    }

    this.view1Sizer = new HorizontalSizer;
    this.view1Sizer.add(this.view1Label)
    this.view1Sizer.add(this.view1List)
    this.view1Sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
    this.view1HueControl = new NumericControl(this);
    this.view1HueControl.label.text = "Hue [°]:";
    this.view1HueControl.label.width = 60;
    this.view1HueControl.setRange(0, 359);
    this.view1HueControl.setPrecision(0);
    this.view1HueControl.slider.setRange(0, 359);
    this.view1HueControl.setValue(gParameters.view1Hue);
    this.view1HueControl.toolTip = "<p>H-alpha hue value.</p>";
    col = convertHSVtoRGB(gParameters.view1Hue, 0.9, 1);
    this.view1HueControl.backgroundColor = Color.rgbaColorF(col[0], col[1], col[2], 1);
    this.view1HueControl.onValueUpdated = function (value) {
        gParameters.view1Hue = value;
        let col = convertHSVtoRGB(value, 0.9, 1);
        this.backgroundColor = Color.rgbaColorF(col[0], col[1], col[2], 1);
    };

    this.view1AmountControl = new NumericControl(this);
    this.view1AmountControl.label.text = "Contribution:";
    this.view1AmountControl.label.width = 60;
    this.view1AmountControl.setRange(0, 1);
    this.view1AmountControl.setPrecision(2);
    this.view1AmountControl.slider.setRange(0, 100);
    this.view1AmountControl.setValue(gParameters.view1Amount);
    this.view1AmountControl.toolTip = "<p>H-alpha contribution.</p>";
    this.view1AmountControl.onValueUpdated = function (value) {
        gParameters.view1Amount = value;
    };

    this.view1GroupBox = new GroupBox(this);
    this.view1GroupBox.title = "H-alpha";
    this.view1GroupBox.sizer = new VerticalSizer;
    this.view1GroupBox.sizer.margin = 8;
    this.view1GroupBox.sizer.spacing = 8;
    this.view1GroupBox.sizer.add(this.view1Sizer);
    this.view1GroupBox.sizer.addSpacing(8);
    this.view1GroupBox.sizer.add(this.view1HueControl);
    this.view1GroupBox.sizer.addSpacing(8);
    this.view1GroupBox.sizer.add(this.view1AmountControl);
    this.view1GroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.view1GroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;


    //  (optional) [OIII] view selection and controls

    this.view2Label = new Label(this);
    this.view2Label.text = "View:";

    this.view2List = new ViewList(this);
    this.view2List.getMainViews();
    this.view2List.remove(View.viewById(OUTPUTVIEWID));
    if (gParameters.view2) {
        this.view2List.currentView = gParameters.view2;
    } else {
        gParameters.view2 = this.view2List.currentView;
    }
    this.view2List.onViewSelected = function (view) {
        gParameters.view2 = view;
    }

    this.view2Sizer = new HorizontalSizer;
    this.view2Sizer.add(this.view2Label)
    this.view2Sizer.add(this.view2List)
    this.view2Sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    this.view2HueControl = new NumericControl(this);
    this.view2HueControl.label.text = "Hue [°]:";
    this.view2HueControl.label.width = 60;
    this.view2HueControl.setRange(0, 359);
    this.view2HueControl.setPrecision(0);
    this.view2HueControl.slider.setRange(0, 359);
    this.view2HueControl.setValue(gParameters.view2Hue);
    this.view2HueControl.toolTip = "<p>[OIII] hue value.</p>";
    col = convertHSVtoRGB(gParameters.view2Hue, 0.9, 1);
    this.view2HueControl.backgroundColor = Color.rgbaColorF(col[0], col[1], col[2], 1);
    this.view2HueControl.onValueUpdated = function (value) {
        gParameters.view2Hue = value;
        let col = convertHSVtoRGB(value, 0.9, 1);
        this.backgroundColor = Color.rgbaColorF(col[0], col[1], col[2], 1);
    };

    this.view2AmountControl = new NumericControl(this);
    this.view2AmountControl.label.text = "Contribution:";
    this.view2AmountControl.label.width = 60;
    this.view2AmountControl.setRange(0, 1);
    this.view2AmountControl.setPrecision(2);
    this.view2AmountControl.slider.setRange(0, 100);
    this.view2AmountControl.setValue(gParameters.view2Amount);
    this.view2AmountControl.toolTip = "<p>[OIII] contribution.</p>";
    this.view2AmountControl.onValueUpdated = function (value) {
        gParameters.view2Amount = value;
    };

    this.view2GroupBox = new GroupBox(this);
    this.view2GroupBox.title = "[OIII] (optional)";
    this.view2GroupBox.sizer = new VerticalSizer;
    this.view2GroupBox.sizer.margin = 8;
    this.view2GroupBox.sizer.spacing = 8;
    this.view2GroupBox.sizer.add(this.view2Sizer);
    this.view2GroupBox.sizer.addSpacing(8);
    this.view2GroupBox.sizer.add(this.view2HueControl);
    this.view2GroupBox.sizer.addSpacing(8);
    this.view2GroupBox.sizer.add(this.view2AmountControl);
    this.view2GroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.view2GroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;

    //  (optional) SII view selection and controls

    this.view3Label = new Label(this);
    this.view3Label.text = "View:";

    this.view3List = new ViewList(this);
    this.view3List.getMainViews();
    this.view3List.remove(View.viewById(OUTPUTVIEWID));
    if (gParameters.view3) {
        this.view3List.currentView = gParameters.view3;
    } else {
        gParameters.view3 = this.view3List.currentView;
    }
    this.view3List.onViewSelected = function (view) {
        gParameters.view3 = view;
    }

    this.view3Sizer = new HorizontalSizer;
    this.view3Sizer.add(this.view3Label)
    this.view3Sizer.add(this.view3List)
    this.view3Sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    this.view3HueControl = new NumericControl(this);
    this.view3HueControl.label.text = "Hue [°]:";
    this.view3HueControl.label.width = 60;
    this.view3HueControl.setRange(0, 359);
    this.view3HueControl.setPrecision(0);
    this.view3HueControl.slider.setRange(0, 359);
    this.view3HueControl.setValue(gParameters.view3Hue);
    this.view3HueControl.toolTip = "<p>SII hue value.</p>";
    col = convertHSVtoRGB(gParameters.view3Hue, 0.9, 1);
    this.view3HueControl.backgroundColor = Color.rgbaColorF(col[0], col[1], col[2], 1);
    this.view3HueControl.onValueUpdated = function (value) {
        gParameters.view3Hue = value;
        let col = convertHSVtoRGB(value, 0.9, 1);
        this.backgroundColor = Color.rgbaColorF(col[0], col[1], col[2], 1);
    };

    this.view3AmountControl = new NumericControl(this);
    this.view3AmountControl.label.text = "Contribution:";
    this.view3AmountControl.label.width = 60;
    this.view3AmountControl.setRange(0, 1);
    this.view3AmountControl.setPrecision(2);
    this.view3AmountControl.slider.setRange(0, 100);
    this.view3AmountControl.setValue(gParameters.view3Amount);
    this.view3AmountControl.toolTip = "<p>SII contribution.</p>";
    this.view3AmountControl.onValueUpdated = function (value) {
        gParameters.view3Amount = value;
    };

    this.view3GroupBox = new GroupBox(this);
    this.view3GroupBox.title = "SII (optional)";
    this.view3GroupBox.sizer = new VerticalSizer;
    this.view3GroupBox.sizer.margin = 8;
    this.view3GroupBox.sizer.spacing = 8;
    this.view3GroupBox.sizer.add(this.view3Sizer);
    this.view3GroupBox.sizer.addSpacing(8);
    this.view3GroupBox.sizer.add(this.view3HueControl);
    this.view3GroupBox.sizer.addSpacing(8);
    this.view3GroupBox.sizer.add(this.view3AmountControl);
    this.view3GroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.view3GroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;

    //  (optional) RGB view selection and controls

    this.view4Label = new Label(this);
    this.view4Label.text = "RGB View:";

    this.view4List = new ViewList(this);
    this.view4List.getMainViews();
    this.view4List.remove(View.viewById(OUTPUTVIEWID));
    if (gParameters.view4) {
        this.view4List.currentView = gParameters.view4;
    } else {
        gParameters.view4 = this.view4List.currentView;
    }
    this.view4List.onViewSelected = function (view) {
        gParameters.view4 = view;
    }

    this.view4Sizer = new HorizontalSizer;
    this.view4Sizer.add(this.view4Label)
    this.view4Sizer.add(this.view4List)
    this.view4Sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    this.view4AmountControl = new NumericControl(this);
    this.view4AmountControl.label.text = "RGB inverse stretch:";
    this.view4AmountControl.label.width = 60;
    this.view4AmountControl.setRange(0.2, 0.99);
    this.view4AmountControl.setPrecision(2);
    this.view4AmountControl.slider.setRange(0, 100);
    this.view4AmountControl.setValue(gParameters.view4Amount);
    this.view4AmountControl.toolTip = "<p>RGB contribution (MTF).</p>";
    this.view4AmountControl.onValueUpdated = function (value) {
        gParameters.view4Amount = value;
    };

    this.view4GroupBox = new GroupBox(this);
    this.view4GroupBox.title = "RGB (optional)";
    this.view4GroupBox.sizer = new VerticalSizer;
    this.view4GroupBox.sizer.margin = 8;
    this.view4GroupBox.sizer.spacing = 8;
    this.view4GroupBox.sizer.add(this.view4Sizer);
    this.view4GroupBox.sizer.addSpacing(8);
    this.view4GroupBox.sizer.add(this.view4AmountControl);
    this.view4GroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.view4GroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;

    //  SCNR control to eliminate the green cast

    this.scnrAmountControl = new NumericControl(this);
    this.scnrAmountControl.label.text = "SCNR Amount:";
    this.scnrAmountControl.label.width = 60;
    this.scnrAmountControl.setRange(0, 1);
    this.scnrAmountControl.setPrecision(2);
    this.scnrAmountControl.slider.setRange(0, 100);
    this.scnrAmountControl.setValue(gParameters.scnrAmount);
    this.scnrAmountControl.toolTip = "<p>Green cast contribution. A value of 1 means no change and 0 stands for full application of SCNR process (Average Neutral)</p>";
    this.scnrAmountControl.onValueUpdated = function (value) {
        gParameters.scnrAmount = value;
    };

    this.scnrAmountGroupBox = new GroupBox(this);
    this.scnrAmountGroupBox.title = "Green cast";
    this.scnrAmountGroupBox.sizer = new VerticalSizer;
    this.scnrAmountGroupBox.sizer.margin = 8;
    this.scnrAmountGroupBox.sizer.spacing = 8;
    this.scnrAmountGroupBox.sizer.add(this.scnrAmountControl);
    this.scnrAmountGroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.scnrAmountGroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;

    // button & checkbox AutoStretch

    this.linkedStretchBox = new CheckBox(this);
    this.linkedStretchBox.text = 'linked STF';
    this.linkedStretchBox.enabled = false;
    this.linkedStretchBox.checked = gParameters.linkedStretch;
    this.linkedStretchBox.toolTip = 'linked ScreenTransferFunction';
    this.linkedStretchBox.onClick = () => {
        gParameters.linkedStretch = this.linkedStretchBox.checked;
    };

    this.autoStretchBox = new CheckBox(this);
    this.autoStretchBox.text = 'Auto STF';
    this.autoStretchBox.checked = gParameters.autoStretch;
    this.autoStretchBox.toolTip = 'Apply ScreenTransferFunction';
    this.autoStretchBox.onClick = () => {
        gParameters.autoStretch = this.autoStretchBox.checked;
        this.linkedStretchBox.enabled = gParameters.autoStretch;
    };

    this.autoStretchSizer = new HorizontalSizer;
    this.autoStretchSizer.add(this.autoStretchBox);
    this.autoStretchSizer.addSpacing(20);
    this.autoStretchSizer.add(this.linkedStretchBox);
    this.autoStretchSizer.addSpacing(20);

    //  message area

    this.messageLabel = new Label(this);
    this.messageLabel.text = "Ready.";
    this.messageLabel.frameStyle = FrameStyle_Sunken;
    this.messageLabel.margin = 4;

    //  lower button row

    this.helpButton = new PushButton(this);
    this.helpButton.text = "Help";
    this.helpButton.icon = this.scaledResource(":/icons/document-text.png");
    this.helpButton.width = 20;
    this.helpButton.onClick = () => {
       Dialog.browseScriptDocumentation("NarrowbandHueCombination");
    };

    this.cancelButton = new PushButton(this);
    this.cancelButton.text = "Cancel";
    this.cancelButton.icon = this.scaledResource(":/icons/cancel.png");
    this.cancelButton.width = 20;
    this.cancelButton.onClick = () => {
        Cleanup();
        this.dialog.cancel();
    };

    this.execButton = new PushButton(this);
    this.execButton.text = "Preview";
    this.execButton.icon = this.scaledResource(":/icons/execute.png");
    this.execButton.width = 20;
    this.execButton.onClick = () => {
        if (gParameters.view1 === undefined || gParameters.view1.isNull) {
            Console.warningln("H-alpha view needs to be specified. OIII, SII and RGB views are optional.");
            this.messageLabel.text = "H-alpha view needs to be specified. OIII, SII and RGB views are optional.";
        } else {
            this.messageLabel.text = "Computing...";
            error = false;

            if (!gParameters.view1.image.isGrayscale
                || (!gParameters.view2.isNull && !gParameters.view2.image.isGrayscale)
                || (!gParameters.view3.isNull && !gParameters.view3.image.isGrayscale)
                || (!gParameters.view4.isNull && gParameters.view4.image.isGrayscale)) {
                this.messageLabel.text = "Error: only grayscale images were accepted!";
                error = true;
            } else {
                if (!gParameters.view2.isNull
                    && ((gParameters.view1.image.width != gParameters.view2.image.width)
                        || (gParameters.view1.image.height != gParameters.view2.image.height))) {
                    this.messageLabel.text = "Error: image geometry is different!";
                    error = true;
                }
                if (!gParameters.view3.isNull
                    && ((gParameters.view1.image.width != gParameters.view3.image.width)
                        || (gParameters.view1.image.height != gParameters.view3.image.height))) {
                    this.messageLabel.text = "Error: image geometry is different!";
                    error = true;
                }
                if (!gParameters.view4.isNull
                    && ((gParameters.view1.image.width != gParameters.view4.image.width)
                        || (gParameters.view1.image.height != gParameters.view4.image.height))) {
                    this.messageLabel.text = "Error: image geometry is different!";
                    error = true;
                }
            }
            if (!error) {
                if (!previewed) {
                    var i = new Image(gParameters.view1.image.width, gParameters.view1.image.height, 3, 1, 32, 1);
                    gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
                    gParameters.workingPreview.mainView.image.assign(i);
                    gParameters.workingPreview.mainView.endProcess();
                }
                previewed = true;
                DoMerge(gParameters.view1, gParameters.view2, gParameters.view3, gParameters.view4, gParameters.view1Hue, gParameters.view2Hue, gParameters.view3Hue, gParameters.view1Amount, gParameters.view2Amount, gParameters.view3Amount, gParameters.view4Amount);
                if (gParameters.autoStretch) StretchPreview(gParameters.workingPreview.mainView, gParameters.linkedStretch);
                var metadata = {
                    width: gParameters.view1.image.width,
                    height: gParameters.view1.image.height
                }
                this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
                this.previewControl.forceRedraw();
                this.messageLabel.text = "Ready.";
            }
        };
    };

    this.doneButton = new PushButton(this);
    this.doneButton.text = "Generate";
    this.doneButton.icon = this.scaledResource(":/icons/ok.png");
    this.doneButton.width = 20;
    this.doneButton.onClick = () => {
        if (gParameters.view1 === undefined || gParameters.view1.isNull) {
            this.messageLabel.text = "H-alpha and OIII views need to be specified, SII view is optional.";
        } else {
            this.messageLabel.text = "Computing...";
            error = false;

            if (!gParameters.view1.image.isGrayscale
                || (!gParameters.view2.isNull && !gParameters.view2.image.isGrayscale)
                || (!gParameters.view3.isNull && !gParameters.view3.image.isGrayscale)
                || (!gParameters.view4.isNull && gParameters.view4.image.isGrayscale)) {
                this.messageLabel.text = "Error: only grayscale images were accepted!";
                error = true;
            } else {
                if (!gParameters.view2.isNull
                    && ((gParameters.view1.image.width != gParameters.view2.image.width)
                        || (gParameters.view1.image.height != gParameters.view2.image.height))) {
                    this.messageLabel.text = "Error: image geometry is different!";
                    error = true;
                }
                if (!gParameters.view3.isNull
                    && ((gParameters.view1.image.width != gParameters.view3.image.width)
                        || (gParameters.view1.image.height != gParameters.view3.image.height))) {
                    this.messageLabel.text = "Error: image geometry is different!";
                    error = true;
                }
                if (!gParameters.view4.isNull
                    && ((gParameters.view1.image.width != gParameters.view4.image.width)
                        || (gParameters.view1.image.height != gParameters.view4.image.height))) {
                    this.messageLabel.text = "Error: image geometry is different!";
                    error = true;
                }
            }
            if (!error) {
                if (!previewed) {
                    var i = new Image(gParameters.view1.image.width, gParameters.view1.image.height, 3, 1, 32, 1);
                    gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
                    gParameters.workingPreview.mainView.image.assign(i);
                    gParameters.workingPreview.mainView.endProcess();
                }
                DoMerge(gParameters.view1, gParameters.view2, gParameters.view3, gParameters.view4, gParameters.view1Hue, gParameters.view2Hue, gParameters.view3Hue, gParameters.view1Amount, gParameters.view2Amount, gParameters.view3Amount, gParameters.view4Amount);
                gParameters.workingPreview.show();
                gParameters.workingPreview.zoomToOptimalFit();
                gParameters.doneButtonPressed = true;
                this.messageLabel.text = "Ready.";
                this.dialog.ok();
            }
        }
    };

    this.buttonSizer = new HorizontalSizer;
    this.buttonSizer.add(this.helpButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.cancelButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.execButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.doneButton);
    this.buttonSizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.buttonSizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

    //  layout the dialog

    this.sizerRight = new VerticalSizer;
    this.sizerRight.margin = 8;
    this.sizerRight.add(this.title);
    this.sizerRight.addStretch();
    this.sizerRight.addSpacing(8);
    this.sizerRight.add(this.view1GroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.view2GroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.view3GroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.view4GroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.scnrAmountGroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.autoStretchSizer);
    this.sizerRight.addSpacing(10);
    this.sizerRight.add(this.messageLabel);
    this.sizerRight.addSpacing(10);
    this.sizerRight.add(this.buttonSizer);
 
    // Add create instance button to the lower left corner
    this.newInstanceButton = new ToolButton(this);
    this.newInstanceButton.icon = this.scaledResource(":/process-interface/new-instance.png");
    this.newInstanceButton.setScaledFixedSize(24, 24);
    this.newInstanceButton.toolTip = "New Instance";
    this.newInstanceButton.onMousePress = () => {
        // stores the parameters
        gParameters.save();
        // create the script instance
        this.newInstance();
    };

    this.newInstanceSizer = new VerticalSizer;
    this.newInstanceSizer.margin = 4;
    this.newInstanceSizer.addStretch();
    this.newInstanceSizer.add(this.newInstanceButton);

    this.sizer = new HorizontalSizer;
    this.sizer.margin = 8;
    this.sizer.add(this.newInstanceSizer);
    this.sizer.add(this.previewControl);
    this.sizer.add(this.sizerRight);
    this.sizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.sizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

}

NarrowbandHueCombinationDialog.prototype = new Dialog;

/*
 * Preview Control
 *
 * This file is part of the AnnotateImage script
 *
 * Copyright (C) 2013-2020, Andres del Pozo
 * Contributions (C) 2019-2020, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <pjsr/ButtonCodes.jsh>
#include <pjsr/StdCursor.jsh>

function PreviewControl(parent) {
    this.__base__ = Frame;
    this.__base__(parent);

    this.SetImage = function (image, metadata) {
        this.image = image;
        this.metadata = metadata;
        this.scaledImage = null;
        this.SetZoomOutLimit();
        this.UpdateZoom(this.zoom);
    };

    this.UpdateZoom = function (newZoom, refPoint) {
        newZoom = Math.max(this.zoomOutLimit, Math.min(4, newZoom));
        if (newZoom == this.zoom && this.scaledImage)
            return;

        if (refPoint == null)
            refPoint = new Point(this.scrollbox.viewport.width / 2, this.scrollbox.viewport.height / 2);

        let imgx = null;
        if (this.scrollbox.maxHorizontalScrollPosition > 0)
            imgx = (refPoint.x + this.scrollbox.horizontalScrollPosition) / this.scale;

        let imgy = null;
        if (this.scrollbox.maxVerticalScrollPosition > 0)
            imgy = (refPoint.y + this.scrollbox.verticalScrollPosition) / this.scale;

        this.zoom = newZoom;
        this.scaledImage = null;
        gc(true);

        if (this.zoom > 0) {
            this.scale = this.zoom;
            this.zoomVal_Label.text = format("%d:1", this.zoom);
        } else {
            this.scale = 1 / (-this.zoom + 2);
            this.zoomVal_Label.text = format("1:%d", -this.zoom + 2);
        }

        if (this.image)
            this.scaledImage = this.image.scaled(this.scale);
        else
            this.scaledImage = {
                width: this.metadata.width * this.scale,
                height: this.metadata.height * this.scale
            };

        this.scrollbox.maxHorizontalScrollPosition = Math.max(0, this.scaledImage.width - this.scrollbox.viewport.width);
        this.scrollbox.maxVerticalScrollPosition = Math.max(0, this.scaledImage.height - this.scrollbox.viewport.height);

        if (this.scrollbox.maxHorizontalScrollPosition > 0 && imgx != null)
            this.scrollbox.horizontalScrollPosition = imgx * this.scale - refPoint.x;
        if (this.scrollbox.maxVerticalScrollPosition > 0 && imgy != null)
            this.scrollbox.verticalScrollPosition = imgy * this.scale - refPoint.y;

        this.scrollbox.viewport.update();
    };

    this.zoomIn_Button = new ToolButton(this);
    this.zoomIn_Button.icon = this.scaledResource(":/icons/zoom-in.png");
    this.zoomIn_Button.setScaledFixedSize(24, 24);
    this.zoomIn_Button.toolTip = "Zoom In";
    this.zoomIn_Button.onMousePress = function () {
        this.parent.UpdateZoom(this.parent.zoom + 1);
    };

    this.zoomOut_Button = new ToolButton(this);
    this.zoomOut_Button.icon = this.scaledResource(":/icons/zoom-out.png");
    this.zoomOut_Button.setScaledFixedSize(24, 24);
    this.zoomOut_Button.toolTip = "Zoom Out";
    this.zoomOut_Button.onMousePress = function () {
        this.parent.UpdateZoom(this.parent.zoom - 1);
    };

    this.zoom11_Button = new ToolButton(this);
    this.zoom11_Button.icon = this.scaledResource(":/icons/zoom-1-1.png");
    this.zoom11_Button.setScaledFixedSize(24, 24);
    this.zoom11_Button.toolTip = "Zoom 1:1";
    this.zoom11_Button.onMousePress = function () {
        this.parent.UpdateZoom(1);
    };

    this.buttons_Sizer = new HorizontalSizer;
    this.buttons_Sizer.margin = 4;
    this.buttons_Sizer.spacing = 4;
    this.buttons_Sizer.add(this.zoomIn_Button);
    this.buttons_Sizer.add(this.zoomOut_Button);
    this.buttons_Sizer.add(this.zoom11_Button);
    this.buttons_Sizer.addStretch();

    this.setScaledMinSize(600, 400);
    this.zoom = 1;
    this.scale = 1;
    this.zoomOutLimit = -5;
    this.scrollbox = new ScrollBox(this);
    this.scrollbox.autoScroll = true;
    this.scrollbox.tracking = true;
    this.scrollbox.cursor = new Cursor(StdCursor_OpenHand);

    this.scroll_Sizer = new HorizontalSizer;
    this.scroll_Sizer.add(this.scrollbox);

    this.SetZoomOutLimit = function () {
        let scaleX = Math.ceil(this.metadata.width / this.scrollbox.viewport.width);
        let scaleY = Math.ceil(this.metadata.height / this.scrollbox.viewport.height);
        let scale = Math.max(scaleX, scaleY);
        this.zoomOutLimit = -scale + 2;
    };

    this.scrollbox.onHorizontalScrollPosUpdated = function (newPos) {
        this.viewport.update();
    };

    this.scrollbox.onVerticalScrollPosUpdated = function (newPos) {
        this.viewport.update();
    };

    this.forceRedraw = function () {
        this.scrollbox.viewport.update();
    };

    this.scrollbox.viewport.onMouseWheel = function (x, y, delta, buttonState, modifiers) {
        let preview = this.parent.parent;
        preview.UpdateZoom(preview.zoom + ((delta > 0) ? -1 : 1), new Point(x, y));
    };

    this.scrollbox.viewport.onMousePress = function (x, y, button, buttonState, modifiers) {
        let preview = this.parent.parent;
        if (preview.scrolling || button != MouseButton_Left)
            return;
        preview.scrolling = {
            orgCursor: new Point(x, y),
            orgScroll: new Point(preview.scrollbox.horizontalScrollPosition, preview.scrollbox.verticalScrollPosition)
        };
        this.cursor = new Cursor(StdCursor_ClosedHand);
    };

    this.scrollbox.viewport.onMouseMove = function (x, y, buttonState, modifiers) {
        let preview = this.parent.parent;
        if (preview.scrolling) {
            preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
            preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
        }

        let ox = (this.parent.maxHorizontalScrollPosition > 0) ?
            -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
        let oy = (this.parent.maxVerticalScrollPosition > 0) ?
            -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
        let coordPx = new Point((x - ox) / preview.scale, (y - oy) / preview.scale);
        /*    if ( coordPx.x < 0 ||
        coordPx.x > preview.metadata.width ||
        coordPx.y < 0 ||
        coordPx.y > preview.metadata.height ||
        !preview.metadata.projection ){
        preview.Xval_Label.text = "---";
        preview.Yval_Label.text = "---";
        preview.RAval_Label.text = "---";
        preview.Decval_Label.text = "---";
        }
        else{
        try{
        preview.Xval_Label.text = format( "%8.2f", coordPx.x );
        preview.Yval_Label.text = format( "%8.2f", coordPx.y );
        let coordRD = preview.metadata.Convert_I_RD( coordPx );
        if ( coordRD.x < 0 )
        coordRD.x += 360;
        preview.RAval_Label.text = DMSangle.FromAngle( coordRD.x*24/360 ).ToString( true );
        preview.Decval_Label.text = DMSangle.FromAngle( coordRD.y ).ToString( false );
        }
        catch ( ex ){
        preview.Xval_Label.text = "---";
        preview.Yval_Label.text = "---";
        preview.RAval_Label.text = "---";
        preview.Decval_Label.text = "---";
        }
        } */
    };

    this.scrollbox.viewport.onMouseRelease = function (x, y, button, buttonState, modifiers) {
        let preview = this.parent.parent;
        if (preview.scrolling && button == MouseButton_Left) {
            preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
            preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
            preview.scrolling = null;
            this.cursor = new Cursor(StdCursor_OpenHand);
        }
    };

    this.scrollbox.viewport.onResize = function (wNew, hNew, wOld, hOld) {
        let preview = this.parent.parent;
        if (preview.metadata && preview.scaledImage) {
            this.parent.maxHorizontalScrollPosition = Math.max(0, preview.scaledImage.width - wNew);
            this.parent.maxVerticalScrollPosition = Math.max(0, preview.scaledImage.height - hNew);
            preview.SetZoomOutLimit();
            preview.UpdateZoom(preview.zoom);
        }
        this.update();
    };

    this.scrollbox.viewport.onPaint = function (x0, y0, x1, y1) {
        let preview = this.parent.parent;
        let graphics = new VectorGraphics(this);

        graphics.fillRect(x0, y0, x1, y1, new Brush(0xff202020));

        let offsetX = (this.parent.maxHorizontalScrollPosition > 0) ?
            -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
        let offsetY = (this.parent.maxVerticalScrollPosition > 0) ?
            -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
        graphics.translateTransformation(offsetX, offsetY);

        if (preview.image)
            graphics.drawBitmap(0, 0, preview.scaledImage);
        else
            graphics.fillRect(0, 0, preview.scaledImage.width, preview.scaledImage.height, new Brush(0xff000000));

        graphics.pen = new Pen(0xffffffff, 0);
        graphics.drawRect(-1, -1, preview.scaledImage.width + 1, preview.scaledImage.height + 1);

        if (preview.onCustomPaint) {
            graphics.antialiasing = true;
            graphics.scaleTransformation(preview.scale, preview.scale);
            preview.onCustomPaint.call(preview.onCustomPaintScope, graphics, x0, y0, x1, y1);
        }

        graphics.end();
    };

    this.zoomLabel_Label = new Label(this);
    this.zoomLabel_Label.text = "Zoom:";
    this.zoomVal_Label = new Label(this);
    this.zoomVal_Label.text = "1:1";


    /*

    this.Xlabel_Label = new Label( this );
    this.Xlabel_Label.text = "X:";
    this.Xval_Label = new Label( this );
    this.Xval_Label.text = "---";

    this.Ylabel_Label = new Label( this );
    this.Ylabel_Label.text = "Y:";
    this.Yval_Label = new Label( this );
    this.Yval_Label.text = "---";

    this.RAlabel_Label = new Label( this );
    this.RAlabel_Label.text = "\u03B1:";
    this.RAval_Label = new Label( this );
    this.RAval_Label.text = "---";

    this.Declabel_Label = new Label( this );
    this.Declabel_Label.text = "\u03B4:";
    this.Decval_Label = new Label( this );
    this.Decval_Label.text = "---";
     */
    this.coords_Frame = new Frame(this);
    this.coords_Frame.styleSheet = this.scaledStyleSheet(
        "QLabel { font-family: Hack; background: white; }");
    this.coords_Frame.backgroundColor = 0xffffffff;
    this.coords_Frame.sizer = new HorizontalSizer;
    this.coords_Frame.sizer.margin = 4;
    this.coords_Frame.sizer.spacing = 4;
    this.coords_Frame.sizer.add(this.zoomLabel_Label);
    this.coords_Frame.sizer.add(this.zoomVal_Label);
    /*    this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.Xlabel_Label );
        this.coords_Frame.sizer.add( this.Xval_Label );
        this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.Ylabel_Label );
        this.coords_Frame.sizer.add( this.Yval_Label );
        this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.RAlabel_Label );
        this.coords_Frame.sizer.add( this.RAval_Label );
        this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.Declabel_Label );
        this.coords_Frame.sizer.add( this.Decval_Label );  */
    this.coords_Frame.sizer.addStretch();

    this.sizer = new VerticalSizer;
    this.sizer.add(this.buttons_Sizer);
    this.sizer.add(this.scroll_Sizer);
    this.sizer.add(this.coords_Frame);
}

PreviewControl.prototype = new Frame;

// helper for hue slider color values
function convertHSVtoRGB(h, s, v) {
    // input: h in [0,360] and s,v in [0,1] - output: r,g,b in [0,1]
    let f = (n, k = (n + h / 60) % 6) => v - v * s * Math.max(Math.min(k, 4 - k, 1), 0);
    return [f(5), f(3), f(1)];
}

// closes preview if script was cancelled
function Cleanup() {
    if (!gParameters.workingPreview.isNull) {
        gParameters.workingPreview.forceClose();
    }
}

// apply autostf stretch
function StretchPreview(view, linked) {
    //
    if (!view.isNull) {
        let autoSTF = new AutoStretch();
        autoSTF.Apply(view, DEFAULT_AUTOSTRETCH_SCLIP, DEFAULT_AUTOSTRETCH_TBGND, linked);
    }
}


// *********
// copied and modified from the old AutoSTF script from Juan Conejero
// *********

function AutoStretch() {
    /*
     * Find a midtones balance value that transforms v1 into v0 through a midtones
     * transfer function (MTF), within the specified tolerance eps.
     */
    this.findMidtonesBalance = function (v0, v1, eps) {
        if (v1 <= 0)
            return 0;

        if (v1 >= 1)
            return 1;

        v0 = Math.range(v0, 0.0, 1.0);

        if (eps)
            eps = Math.max(1.0e-15, eps);
        else
            eps = 5.0e-05;

        var m0,
            m1;
        if (v1 < v0) {
            m0 = 0;
            m1 = 0.5;
        } else {
            m0 = 0.5;
            m1 = 1;
        }

        for (; ;) {
            var m = (m0 + m1) / 2;
            var v = Math.mtf(m, v1);

            if (Math.abs(v - v0) < eps)
                return m;

            if (v < v0)
                m1 = m;
            else
                m0 = m;
        }
    }

    /*
     * STF Auto Stretch routine
     */
    this.Apply = function (view, shadowsClipping, targetBackground, linked) {

        var ht = new HistogramTransformation;

        var n = view.image.isColor ? 3 : 1;

        if (linked) {

            var c0 = 0;
            var m = 0;
            for (var c = 0; c < n; ++c) {
                view.image.selectedChannel = c;
                var median = view.image.median();
                var avgDev = view.image.avgDev();
                c0 += median + shadowsClipping * avgDev;
                m += median;
            }
            view.image.resetSelections();
            c0 = Math.range(c0 / n, 0.0, 1.0);
            m = Math.mtf(targetBackground, m / n - c0);

            ht.H = [[0, 0.5, 1, 0, 1],
            [0, 0.5, 1, 0, 1],
            [0, 0.5, 1, 0, 1],
            [c0, m, 1, 0, 1],
            [0, 0.5, 1, 0, 1]];

        } else {

            var A = [// c0, c1, m, r0, r1
                [0, 0.5, 1, 0, 1],
                [0, 0.5, 1, 0, 1],
                [0, 0.5, 1, 0, 1],
                [0, 0.5, 1, 0, 1]];

            for (var c = 0; c < n; ++c) {
                view.image.selectedChannel = c;
                var median = view.image.median();
                var avgDev = view.image.avgDev();
                var c0 = Math.range(median + shadowsClipping * avgDev, 0.0, 1.0);
                var m = Math.mtf(targetBackground, median - c0);
                A[c] = [c0, m, 1, 0, 1];
            }

            ht.H = A;

            view.image.resetSelections();
        }
        ht.executeOn(view, false);
    }
};

function main() {

    // ImageWindow
    gParameters.imageActiveWindow = new ImageWindow(ImageWindow.activeWindow);

    // Validations
    if (gParameters.imageActiveWindow === undefined) {

        console.show();
        Console.warningln("<p>No active image.</p>");

    } else if (gParameters.imageActiveWindow.currentView.isPreview) {

        console.show();
        Console.warningln("<p>This script can not work on prewiews</p>");

    } else {

        // is script started from an instance in global or view target context?
        if (Parameters.isGlobalTarget || Parameters.isViewTarget) {
            // then load the parameters from the instance and continue
            gParameters.load();
        }

        // create and show the dialog
        let dialog = new NarrowbandHueCombinationDialog;
        dialog.execute();
        if (!gParameters.doneButtonPressed) {
            Cleanup();
            dialog.cancel();
        }
    }

}

main();
