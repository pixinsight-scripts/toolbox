\documentclass PIScriptDoc

\script CombineRGBAndNarrowband

\keywords {
Combine, Images, RGB, Narrowband
}

\author {
Juergen Terpe
}

\copyright {
2023 Juergen Terpe
}

\brief {
Use the CombineRGBAndNarrowband script to combine RGB images and Narrowband images captured with a one shot color camera.
The script is intended for beeing used on non-linear images.
}

\description {
Use this script for combining non-linear RGB images and starless, non-linear Narrowband images using an additional star mask. The natural sky background and the overall impression of the RGB image are preserved and are only additionally enriched and strengthened by the additional H-Alpha and OIII data. If the RGB image still contains stars, these should be protected by an additional star mask.

The non-linear RGB image should be processed and stretched with the normal workflow. A star mask can then be created using StarXTerminator or StartNet, for example. Simply convert the extracted stars to a grayscale image and please do not do any further processing, especially do not add any blur to the image as is commonly done.
The Narrowband image should also be processed and stretched in the normal way. Optionally you can try to use an AutoLinearFit instead of the normal color calibration, which might make the blue color part more visible in the result. This image should already be processed so far that it has an optimal contrast and sharpness. In the Narrowband image, the stars must be removed before applying the script.
Finally, use the ChannelExtraction process to extract the red, green and blue channel from your narrowband image.

The script expects an image with existing natural RGB sky background.
In the case of large nebula regions and a smaller image section, it is possible that there is only little or no background sky left. In these cases you should either abandon the use of this script or set the blackpoint sliders for OIII and H-Alpha both to 0.

\note {
This script is a tool designed to produce a nice looking image, not a scientifically usable result. The user of the script is responsible for using the available settings to produce a realistic image, just as he or she does with stretching and all the other processes in PixInsight. Just as with other processes, too much can lead to completely exaggerated and unnatural-looking images. This script as a tool is not intended to tell the user how to edit his image.
}
}

\usage {
   \list[spaced] {

      { \s {RGB Image}: \nf
         Select the RGB image. This image must be color-calibrated and stretched. You can use a starless image or if you prefer
         to process your image with stars included you should provide a star mask for this RGB image.
      }
      { \s {Narrowband Red}: \nf
         Select the red narrowband image channel. This image must be starless and stretched. You can use the script "AutoLinearFit" for
         a rough color calibration or process it to meet your taste, but the stars must be removed and the image must be
         stretched. This must be a grayscale image.
      }

      { \s {Narrowband Green}: \nf
         Select the green narrowband image channel. This image must be starless and stretched. You can use the script "AutoLinearFit" for
         a rough color calibration or process it to meet your taste, but the stars must be removed and the image must be
         stretched. This must be a grayscale image.
      }

      { \s {Narrowband Blue}: \nf
         Select the blue narrowband image channel. This image must be starless and stretched. You can use the script "AutoLinearFit" for
         a rough color calibration or process it to meet your taste, but the stars must be removed and the image must be
         stretched. This must be a grayscale image.
      }


      { \s {Star Mask}: \nf
         Select a star mask created using the RGB image here. The star mask can be created using StarXTerminator or any other tool of your choice. Don't blur this star mask! It should be a grayscale version of the extracted stars without any further processing. The star mask will be subtracted from both the H-Alpha and the OIII signal to avoid wrong star colors.
      }

      { \s {Blackpoints}: \nf
         \list[spaced] {
            { \s {Red}: \nf
               Change the blackpoint of the red contribution. The value "1.0" means the median of the red contribution extracted from by the script from your narrowband image. When you have much faint red signal in your image or the image contains only a region filled entirely with nebulosity, you must choose a smaller value or even "0.0".
            }
            { \s {Green}: \nf
               Change the blackpoint of the green contribution. The value "1.0" means the median of the green contribution extracted from by the script from your narrowband image. When you have much faint green signal in your image or the image contains only a region filled entirely with nebulosity, you must choose a smaller value or even "0.0".
            }
            { \s {Blue}: \nf
               Change the blackpoint of the blue contribution. The value "1.0" means the median of the blue contribution extracted from by the script from your narrowband image. When you have much faint blue signal in your image or the image contains only a region filled entirely with nebulosity, you must choose a smaller value or even "0.0".
            }
         }
      }

      { \s {Balance}: \nf
         \list[spaced] {
            { \s {Amount}: \nf
               Controls the amount of total intensity for the added red, green and blue channels. Decrease to get a less strong signal, increasing the amount will make the signal stronger. The amount always works for all channels and helps to avoid oversaturation.
            }
            { \s {Vibrance}: \nf
               Controls the color vibrance (the mid-tones). Increase this value to intensify the color, decrease it to have less color saturation in the mid-tones.
            }
            { \s {Highlights}: \nf
               Controls the highlights of the image. Decrease the highlights to compensate for oversaturation or the have a more natural look for your target.
            }
            { \s {Stars}: \nf
               The star mask will be subtracted from the red, green and blue channels with this factor. Find a value where all your stars have a natural look and color. Also check for dark outlines around the stars and decrease this value to avoid these outlines.
            }
            { \s {Rescale}: \nf
               When using a starless RGB image without a star mask you can just use rescale and decrease this value to avoid oversaturation. With stars within the RGB image rescaling will not make much sense.
            }
         }
      }

      { \s {Update the preview}: \nf
         Use this button to update the preview when you are done with your changes to have a look how the final image might look like.
      }
   }
}

\make
