<?xml version="1.0" encoding="UTF-8"?>
<xri version="1.0">
   <description>
      <p>
        A collection of PixInsight scripts for image processing. The following scripts are
        currently contained: 
        	- CreateHubblePaletteFromOSC: Creates a Hubble palette image from a linear image captured using a One shot camera using dual narrowband filters.
        	- ImproveBrilliance: Improves the brilliance of color images.
        	- CombineImages: Combines images or masks with different methods.  
        	- ContinuumSubtraction: Continuum subtraction from Narrowband images to extract Ha.
        	- CombineHaToRGB: Combine Ha with RGB image.
        	- CreateHDRImage: Integrate HDR into your images for brighter regions of your target to show more details.
        	- AutoLinearFit: Linear Fit on all color channels automatically.  
        	- EnhanceNebula: Brightens the nebula. 
        	- SelectiveColorCorrection: Selectively change the colors of your image. 
        	- LabColorBoost: Improves the colors of your image using the L*a*b color space.
        	- CombineRGBAndNarrowband: Combines RGB image with duo narrowband image.
     </p>
   </description>
   <platform os="all" arch="noarch" version="1.8.9-1:1.9.10">
      <package fileName="20240114-PixInsightToolbox-Package.zip" sha1="b7048546a1ff17f6ec7c4d089c6cd9a92f50af4c" type="script" releaseDate="20240114">
         <title>
            PixInsight Utility scripts
         </title>
         <remove>
            src/scripts/AutoLinearFit.js,
            src/scripts/AutoLinearFit.xsgn,
            src/scripts/CombineRGBAndDuoNarrowband.js,
            src/scripts/CombineRGBAndDuoNarrowband.xsgn,
            src/scripts/CombineHaToRGB.js,
            src/scripts/CombineHaToRGB.xsgn,
            src/scripts/CombineImages.js,
            src/scripts/CombineImages.xsgn,
            src/scripts/ContinuumSubtraction.js,
            src/scripts/ContinuumSubtraction.xsgn,
            src/scripts/CreateHDRImage.js,
            src/scripts/CreateHDRImage.xsgn,
            src/scripts/CreateHubblePaletteFromOSC.js,
            src/scripts/CreateHubblePaletteFromOSC.xsgn,
            src/scripts/EnhanceNebula.js,
            src/scripts/EnhanceNebula.xsgn,
            src/scripts/ImproveBrilliance.js,
            src/scripts/ImproveBrilliance.xsgn,
            src/scripts/LabColorBoost.js,
            src/scripts/LabColorBoost.xsgn,
            src/scripts/PixInsightToolsPreviewControl.jsh,
            src/scripts/PixInsightToolsPreviewControl.xsgn,
            src/scripts/SelectiveColorCorrection.js,
            src/scripts/SelectiveColorCorrection.xsgn,
            doc/scripts/CombineRGBAndDuoNarrowband
         </remove>
         <description>
            <p>SelectiveColorCorrection: <br/>
            - Increased ranges for editing masks. <br/>
            - New UNDO button to undo applied changes on images. <br/>
            </p>
            <p>GraXpert 1.2c: <br/>
            	Script execution over the process icon in a global context now reopens the GUI as requested by some users. <br/>
            	<b>You will need to install GraXpert release 2.2.0 or higher in order to use this script! The old script version will also not work with the new GraXpert version.</b>    
            </p>
            <p>Copyright (c) 2024 Juergen Terpe, All Rights Reserved.</p>
         </description>
      </package>
   </platform>
</xri>
<Signature developerId="JuergenTerpe" timestamp="2024-01-14T14:38:46.807Z" encoding="Base64">TjzKQAtUrDHAWH4hgg595FTQU9/+RCtt1bd+V3WwcRwlqomUvBTLO1pA0yLKSzpQvUXxEC+Ni0Tz93HMdMxDCA==</Signature>
